{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.05";
    flake-utils.url = "github:numtide/flake-utils";
    nur-kapack = {
      url = "github:oar-team/nur-kapack/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    resourceset = {
      url = "git+https://framagit.org/batsim/intervalset?ref=generic-base";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batprotocol = {
      url = "git+https://framagit.org/batsim/batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, nur-kapack, resourceset, batprotocol }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };
        kapack = nur-kapack.packages.${system};
        cppMesonDevBase = nur-kapack.lib.${system}.cppMesonDevBase;
        release-options = {
          debug = false;
          doCoverage = false;
          batprotocol-cpp = batprotocol.packages-release.${system}.batprotocol-cpp;
          resourceset = resourceset.packages-release.${system}.resourceset;
        };
        debug-options = {
          debug = true;
          doCoverage = false;
          batprotocol-cpp = batprotocol.packages-debug.${system}.batprotocol-cpp;
          resourceset = resourceset.packages-debug.${system}.resourceset;
        };
        base-defs = {
          cppMesonDevBase = nur-kapack.lib.${system}.cppMesonDevBase;
        };
        callPackage = mergedPkgs: deriv-func: attrset: options: pkgs.lib.callPackageWith(mergedPkgs // options) deriv-func attrset;
      in rec {
        functions = rec {
          edc = import ./edc.nix;
          generate-packages = mergedPkgs: options: {
            edc = callPackage mergedPkgs edc {} options;
          };
        };
        packages-release = functions.generate-packages (pkgs // base-defs // packages-release) release-options;
        packages-debug = functions.generate-packages (pkgs // base-defs // packages-debug) debug-options;
        packages = packages-release;
      }
    );
}
